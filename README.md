# Agora-software

Inside the folder "Agora(DesktopVersion)" you can find all the Max, Ableton and CataRT patches for this performance. 

---- 

Agorá is a live performance for the Pointing-at data glove performed and developed between 2012 and 2014
Here you can find the software I developed for it (Max, Ableton, CataRT).

You can find a video of this performance [HERE](https://vimeo.com/44650248)

This is also part of my PhD research (completed in 2013) of which further details can be found at links below:

* [PhD Dissertation](https://ulir.ul.ie/handle/10344/3285)
* [Video Documentary](https://vimeo.com/52557261)
* [AHRS Max Library](https://gitlab.com/giuseppetorre/ahrs)
