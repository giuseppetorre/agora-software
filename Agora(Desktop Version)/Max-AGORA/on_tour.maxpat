{
	"patcher" : 	{
		"fileversion" : 1,
		"rect" : [ 14.0, 59.0, 712.0, 672.0 ],
		"bglocked" : 0,
		"defrect" : [ 14.0, 59.0, 712.0, 672.0 ],
		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"boxes" : [ 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : "1",
					"fontsize" : 9.0,
					"numinlets" : 2,
					"numoutlets" : 1,
					"patching_rect" : [ 594.0, 151.0, 16.0, 15.0 ],
					"outlettype" : [ "" ],
					"id" : "obj-1",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "loadbang",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"patching_rect" : [ 594.0, 113.0, 48.0, 17.0 ],
					"outlettype" : [ "bang" ],
					"id" : "obj-2",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : ";\rmax launchbrowser http://www.ngdc.noaa.gov/geomagmodels/IGRFWMM.jsp",
					"linecount" : 2,
					"fontsize" : 9.0,
					"numinlets" : 2,
					"numoutlets" : 1,
					"patching_rect" : [ 62.0, 52.0, 359.0, 25.0 ],
					"outlettype" : [ "" ],
					"id" : "obj-3",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"fontsize" : 18.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"hbgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"patching_rect" : [ 402.0, 215.0, 61.0, 27.0 ],
					"outlettype" : [ "float", "bang" ],
					"id" : "obj-4",
					"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
					"fontname" : "Arial",
					"bordercolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"triscale" : 0.9,
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"fontsize" : 18.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"hbgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"patching_rect" : [ 310.0, 215.0, 61.0, 27.0 ],
					"outlettype" : [ "float", "bang" ],
					"id" : "obj-5",
					"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
					"fontname" : "Arial",
					"bordercolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"triscale" : 0.9,
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "s dec_inc_dist",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 126.0, 509.0, 75.0, 17.0 ],
					"id" : "obj-6",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "z",
					"fontsize" : 14.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 418.0, 260.0, 20.0, 23.0 ],
					"id" : "obj-7",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "x",
					"fontsize" : 14.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 473.0, 312.0, 20.0, 23.0 ],
					"id" : "obj-8",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "y",
					"fontsize" : 14.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 273.0, 260.0, 20.0, 23.0 ],
					"id" : "obj-9",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "x",
					"fontsize" : 14.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 325.0, 312.0, 20.0, 23.0 ],
					"id" : "obj-10",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "z",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 319.0, 523.0, 17.0, 17.0 ],
					"id" : "obj-11",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "y",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 277.0, 523.0, 17.0, 17.0 ],
					"id" : "obj-12",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "x",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 230.0, 523.0, 17.0, 17.0 ],
					"id" : "obj-13",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "p visualize",
					"fontsize" : 9.0,
					"numinlets" : 2,
					"numoutlets" : 1,
					"patching_rect" : [ 218.0, 566.0, 91.0, 17.0 ],
					"outlettype" : [ "" ],
					"id" : "obj-14",
					"fontname" : "Arial",
					"patcher" : 					{
						"fileversion" : 1,
						"rect" : [ 10.0, 59.0, 295.0, 258.0 ],
						"bglocked" : 0,
						"defrect" : [ 10.0, 59.0, 295.0, 258.0 ],
						"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 0,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"boxes" : [ 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "p stuff",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"patching_rect" : [ 50.0, 122.0, 40.0, 17.0 ],
									"outlettype" : [ "", "" ],
									"id" : "obj-1",
									"fontname" : "Arial",
									"patcher" : 									{
										"fileversion" : 1,
										"rect" : [ 231.0, 88.0, 302.0, 407.0 ],
										"bglocked" : 0,
										"defrect" : [ 231.0, 88.0, 302.0, 407.0 ],
										"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 0,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 0,
										"toolbarvisible" : 1,
										"boxanimatetime" : 200,
										"imprint" : 0,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"boxes" : [ 											{
												"box" : 												{
													"maxclass" : "newobj",
													"text" : "p lcd",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"numoutlets" : 1,
													"patching_rect" : [ 46.0, 235.0, 30.0, 17.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-1",
													"fontname" : "Arial",
													"patcher" : 													{
														"fileversion" : 1,
														"rect" : [ 40.0, 44.0, 982.0, 502.0 ],
														"bglocked" : 0,
														"defrect" : [ 40.0, 44.0, 982.0, 502.0 ],
														"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
														"openinpresentation" : 0,
														"default_fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"gridonopen" : 0,
														"gridsize" : [ 15.0, 15.0 ],
														"gridsnaponopen" : 0,
														"toolbarvisible" : 1,
														"boxanimatetime" : 200,
														"imprint" : 0,
														"enablehscroll" : 1,
														"enablevscroll" : 1,
														"devicewidth" : 0.0,
														"boxes" : [ 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "p angle-conversion",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 570.0, 150.0, 95.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-1",
																	"fontname" : "Arial",
																	"patcher" : 																	{
																		"fileversion" : 1,
																		"rect" : [ 216.0, 130.0, 208.0, 267.0 ],
																		"bglocked" : 0,
																		"defrect" : [ 216.0, 130.0, 208.0, 267.0 ],
																		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
																		"openinpresentation" : 0,
																		"default_fontsize" : 12.0,
																		"default_fontface" : 0,
																		"default_fontname" : "Arial",
																		"gridonopen" : 0,
																		"gridsize" : [ 15.0, 15.0 ],
																		"gridsnaponopen" : 0,
																		"toolbarvisible" : 1,
																		"boxanimatetime" : 200,
																		"imprint" : 0,
																		"enablehscroll" : 1,
																		"enablevscroll" : 1,
																		"devicewidth" : 0.0,
																		"boxes" : [ 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "- 180",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 119.0, 35.0, 17.0 ],
																					"outlettype" : [ "int" ],
																					"id" : "obj-1",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "* 180.",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 95.0, 40.0, 17.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-2",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "/ 3.141594",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 72.0, 63.0, 17.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-3",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "+ 3.141594",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 50.0, 63.0, 17.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-4",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "inlet",
																					"numinlets" : 0,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 30.0, 15.0, 15.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-5",
																					"comment" : ""
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "outlet",
																					"numinlets" : 1,
																					"numoutlets" : 0,
																					"patching_rect" : [ 50.0, 141.0, 15.0, 15.0 ],
																					"id" : "obj-6",
																					"comment" : ""
																				}

																			}
 ],
																		"lines" : [ 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-1", 0 ],
																					"destination" : [ "obj-6", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-2", 0 ],
																					"destination" : [ "obj-1", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-3", 0 ],
																					"destination" : [ "obj-2", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-4", 0 ],
																					"destination" : [ "obj-3", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-5", 0 ],
																					"destination" : [ "obj-4", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
 ]
																	}
,
																	"saved_object_attributes" : 																	{
																		"fontface" : 0,
																		"fontsize" : 12.0,
																		"default_fontface" : 0,
																		"default_fontname" : "Arial",
																		"fontname" : "Arial",
																		"default_fontsize" : 12.0,
																		"globalpatchername" : ""
																	}

																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "clear",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 874.0, 351.0, 33.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-2",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "t clear",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 851.0, 71.0, 40.0, 17.0 ],
																	"outlettype" : [ "clear" ],
																	"id" : "obj-3",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "t b",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 485.0, 290.0, 20.0, 17.0 ],
																	"outlettype" : [ "bang" ],
																	"id" : "obj-4",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "linesegment 70 0 70 140 246, linesegment 0 70 140 70 246, pensize 10 10",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 485.0, 316.0, 355.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-5",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "pensize 1 1",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 190.0, 340.0, 60.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-6",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "t b",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 190.0, 204.0, 20.0, 17.0 ],
																	"outlettype" : [ "bang" ],
																	"id" : "obj-7",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "pensize 2 2",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 190.0, 320.0, 60.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-8",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend paintarc",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 643.0, 342.0, 84.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-9",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "$1 $2 $3 $4 $5 $6 7",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 643.0, 292.0, 105.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-10",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "$1 $2 70 $2 248, $1 $2 $1 70 248",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 260.0, 203.0, 173.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-11",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "pensize 1 1",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 413.0, 342.0, 60.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-12",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 0 0 90 360 28",
																	"fontsize" : 9.0,
																	"numinlets" : 7,
																	"numoutlets" : 1,
																	"patching_rect" : [ 485.0, 251.0, 115.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-13",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend framearc",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 550.0, 342.0, 89.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-14",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 413.0, 99.0, 30.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-15",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 99.0, 29.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-16",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "cartopol",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 2,
																	"patching_rect" : [ 378.0, 121.0, 45.0, 17.0 ],
																	"outlettype" : [ "float", "float" ],
																	"id" : "obj-17",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 483.0, 154.0, 31.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-18",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 448.0, 154.0, 30.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-19",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "!- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 413.0, 154.0, 33.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-20",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "!- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 154.0, 32.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-21",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 0 0 255",
																	"fontsize" : 9.0,
																	"numinlets" : 5,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 176.0, 153.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-22",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend frameoval",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 251.0, 93.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-23",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 70 70 212",
																	"fontsize" : 9.0,
																	"numinlets" : 5,
																	"numoutlets" : 1,
																	"patching_rect" : [ 251.0, 154.0, 99.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-24",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend linesegment",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 251.0, 251.0, 100.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-25",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 138.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-26",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 109.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-27",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 80.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-28",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-29",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 0 0 255",
																	"fontsize" : 9.0,
																	"numinlets" : 5,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 185.0, 127.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-30",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend paintoval",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 251.0, 88.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-31",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "unpack",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"patching_rect" : [ 51.0, 65.0, 39.0, 17.0 ],
																	"outlettype" : [ "int", "int" ],
																	"id" : "obj-32",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "outlet",
																	"numinlets" : 1,
																	"numoutlets" : 0,
																	"patching_rect" : [ 51.0, 395.0, 15.0, 15.0 ],
																	"id" : "obj-33",
																	"comment" : ""
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "inlet",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 31.0, 15.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-34",
																	"comment" : ""
																}

															}
 ],
														"lines" : [ 															{
																"patchline" : 																{
																	"source" : [ "obj-34", 0 ],
																	"destination" : [ "obj-3", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 60.0, 54.0, 860.5, 54.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-10", 0 ],
																	"destination" : [ "obj-9", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-13", 0 ],
																	"destination" : [ "obj-10", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-1", 0 ],
																	"destination" : [ "obj-13", 5 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 1 ],
																	"destination" : [ "obj-1", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-13", 0 ],
																	"destination" : [ "obj-14", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-18", 0 ],
																	"destination" : [ "obj-13", 3 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-19", 0 ],
																	"destination" : [ "obj-13", 2 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-20", 0 ],
																	"destination" : [ "obj-13", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-4", 0 ],
																	"destination" : [ "obj-5", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-13", 0 ],
																	"destination" : [ "obj-4", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-21", 0 ],
																	"destination" : [ "obj-13", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-18", 0 ],
																	"destination" : [ "obj-22", 3 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-18", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 146.0, 492.5, 146.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-19", 0 ],
																	"destination" : [ "obj-22", 2 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-19", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 146.0, 457.5, 146.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-4", 0 ],
																	"destination" : [ "obj-12", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 494.5, 311.0, 422.5, 311.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-20", 0 ],
																	"destination" : [ "obj-22", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-20", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 146.0, 422.5, 146.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-15", 0 ],
																	"destination" : [ "obj-17", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-15", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-22", 0 ],
																	"destination" : [ "obj-23", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-21", 0 ],
																	"destination" : [ "obj-22", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-21", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-16", 0 ],
																	"destination" : [ "obj-17", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-16", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-24", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-24", 0 ],
																	"destination" : [ "obj-11", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 260.5, 185.0, 269.5, 185.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-24", 0 ],
																	"destination" : [ "obj-25", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-11", 0 ],
																	"destination" : [ "obj-25", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 269.5, 236.0, 260.5, 236.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-24", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-7", 0 ],
																	"destination" : [ "obj-6", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-7", 0 ],
																	"destination" : [ "obj-8", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-24", 0 ],
																	"destination" : [ "obj-7", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 260.5, 177.0, 199.5, 177.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-26", 0 ],
																	"destination" : [ "obj-30", 3 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-26", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-27", 0 ],
																	"destination" : [ "obj-30", 2 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-27", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-28", 0 ],
																	"destination" : [ "obj-30", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-28", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-2", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 883.5, 386.0, 60.0, 386.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-3", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 860.5, 386.0, 60.0, 386.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-5", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 494.5, 371.0, 60.0, 371.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-8", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 199.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-6", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 199.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-9", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 652.5, 371.0, 60.0, 371.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-12", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 422.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-14", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 559.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-23", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-25", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 260.5, 371.0, 60.0, 371.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-31", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-30", 0 ],
																	"destination" : [ "obj-31", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-29", 0 ],
																	"destination" : [ "obj-30", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-29", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-34", 0 ],
																	"destination" : [ "obj-32", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
 ]
													}
,
													"saved_object_attributes" : 													{
														"fontface" : 0,
														"fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"fontname" : "Arial",
														"default_fontsize" : 12.0,
														"globalpatchername" : ""
													}

												}

											}
, 											{
												"box" : 												{
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"patching_rect" : [ 46.0, 50.0, 15.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-2",
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 219.0, 308.0, 15.0, 15.0 ],
													"id" : "obj-3",
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 172.0, 308.0, 15.0, 15.0 ],
													"id" : "obj-4",
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "0 0",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"numoutlets" : 1,
													"patching_rect" : [ 219.0, 81.0, 23.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-5",
													"fontname" : "Arial"
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "70 70",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"numoutlets" : 1,
													"patching_rect" : [ 129.0, 81.0, 37.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-6",
													"fontname" : "Arial"
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "newobj",
													"text" : "loadbang",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"numoutlets" : 1,
													"patching_rect" : [ 129.0, 50.0, 48.0, 17.0 ],
													"outlettype" : [ "bang" ],
													"id" : "obj-7",
													"fontname" : "Arial"
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "local 0",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"numoutlets" : 1,
													"patching_rect" : [ 172.0, 81.0, 39.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-8",
													"fontname" : "Arial"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"source" : [ "obj-5", 0 ],
													"destination" : [ "obj-3", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-7", 0 ],
													"destination" : [ "obj-5", 0 ],
													"hidden" : 0,
													"midpoints" : [ 138.5, 73.0, 228.5, 73.0 ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-1", 0 ],
													"destination" : [ "obj-4", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-8", 0 ],
													"destination" : [ "obj-4", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-7", 0 ],
													"destination" : [ "obj-8", 0 ],
													"hidden" : 0,
													"midpoints" : [ 138.5, 73.0, 181.5, 73.0 ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-7", 0 ],
													"destination" : [ "obj-6", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-6", 0 ],
													"destination" : [ "obj-1", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-2", 0 ],
													"destination" : [ "obj-1", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
 ]
									}
,
									"saved_object_attributes" : 									{
										"fontface" : 0,
										"fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"fontname" : "Arial",
										"default_fontsize" : 12.0,
										"globalpatchername" : ""
									}

								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "pak 0 0",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"numoutlets" : 1,
									"patching_rect" : [ 50.0, 94.0, 120.0, 17.0 ],
									"outlettype" : [ "" ],
									"id" : "obj-2",
									"fontname" : "Arial"
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "scale -1. 1. 140 0",
									"fontsize" : 9.0,
									"numinlets" : 6,
									"numoutlets" : 1,
									"patching_rect" : [ 160.0, 66.0, 92.0, 17.0 ],
									"outlettype" : [ "" ],
									"id" : "obj-3",
									"fontname" : "Arial"
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "scale -1. 1. 0 140",
									"fontsize" : 9.0,
									"numinlets" : 6,
									"numoutlets" : 1,
									"patching_rect" : [ 50.0, 66.0, 92.0, 17.0 ],
									"outlettype" : [ "" ],
									"id" : "obj-4",
									"fontname" : "Arial"
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"patching_rect" : [ 160.0, 30.0, 15.0, 15.0 ],
									"outlettype" : [ "float" ],
									"id" : "obj-5",
									"comment" : ""
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"patching_rect" : [ 50.0, 30.0, 15.0, 15.0 ],
									"outlettype" : [ "float" ],
									"id" : "obj-6",
									"comment" : ""
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 159.0, 15.0, 15.0 ],
									"id" : "obj-7",
									"comment" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"source" : [ "obj-3", 0 ],
									"destination" : [ "obj-2", 1 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-5", 0 ],
									"destination" : [ "obj-3", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-1", 0 ],
									"destination" : [ "obj-7", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-2", 0 ],
									"destination" : [ "obj-1", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-4", 0 ],
									"destination" : [ "obj-2", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-6", 0 ],
									"destination" : [ "obj-4", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
 ]
					}
,
					"saved_object_attributes" : 					{
						"fontface" : 0,
						"fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"fontname" : "Arial",
						"default_fontsize" : 12.0,
						"globalpatchername" : ""
					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "lcd",
					"numinlets" : 1,
					"numoutlets" : 4,
					"patching_rect" : [ 202.0, 259.0, 140.0, 140.0 ],
					"outlettype" : [ "list", "list", "int", "" ],
					"id" : "obj-15",
					"local" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "p visualize",
					"fontsize" : 9.0,
					"numinlets" : 2,
					"numoutlets" : 1,
					"patching_rect" : [ 363.0, 566.0, 91.0, 17.0 ],
					"outlettype" : [ "" ],
					"id" : "obj-16",
					"fontname" : "Arial",
					"patcher" : 					{
						"fileversion" : 1,
						"rect" : [ 10.0, 59.0, 295.0, 258.0 ],
						"bglocked" : 0,
						"defrect" : [ 10.0, 59.0, 295.0, 258.0 ],
						"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 0,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"boxes" : [ 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "p stuff",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"numoutlets" : 2,
									"patching_rect" : [ 50.0, 122.0, 40.0, 17.0 ],
									"outlettype" : [ "", "" ],
									"id" : "obj-1",
									"fontname" : "Arial",
									"patcher" : 									{
										"fileversion" : 1,
										"rect" : [ 231.0, 88.0, 302.0, 407.0 ],
										"bglocked" : 0,
										"defrect" : [ 231.0, 88.0, 302.0, 407.0 ],
										"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 0,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 0,
										"toolbarvisible" : 1,
										"boxanimatetime" : 200,
										"imprint" : 0,
										"enablehscroll" : 1,
										"enablevscroll" : 1,
										"devicewidth" : 0.0,
										"boxes" : [ 											{
												"box" : 												{
													"maxclass" : "newobj",
													"text" : "p lcd",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"numoutlets" : 1,
													"patching_rect" : [ 46.0, 235.0, 30.0, 17.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-1",
													"fontname" : "Arial",
													"patcher" : 													{
														"fileversion" : 1,
														"rect" : [ 40.0, 44.0, 982.0, 502.0 ],
														"bglocked" : 0,
														"defrect" : [ 40.0, 44.0, 982.0, 502.0 ],
														"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
														"openinpresentation" : 0,
														"default_fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"gridonopen" : 0,
														"gridsize" : [ 15.0, 15.0 ],
														"gridsnaponopen" : 0,
														"toolbarvisible" : 1,
														"boxanimatetime" : 200,
														"imprint" : 0,
														"enablehscroll" : 1,
														"enablevscroll" : 1,
														"devicewidth" : 0.0,
														"boxes" : [ 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "p angle-conversion",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 570.0, 150.0, 95.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-1",
																	"fontname" : "Arial",
																	"patcher" : 																	{
																		"fileversion" : 1,
																		"rect" : [ 216.0, 130.0, 208.0, 267.0 ],
																		"bglocked" : 0,
																		"defrect" : [ 216.0, 130.0, 208.0, 267.0 ],
																		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
																		"openinpresentation" : 0,
																		"default_fontsize" : 12.0,
																		"default_fontface" : 0,
																		"default_fontname" : "Arial",
																		"gridonopen" : 0,
																		"gridsize" : [ 15.0, 15.0 ],
																		"gridsnaponopen" : 0,
																		"toolbarvisible" : 1,
																		"boxanimatetime" : 200,
																		"imprint" : 0,
																		"enablehscroll" : 1,
																		"enablevscroll" : 1,
																		"devicewidth" : 0.0,
																		"boxes" : [ 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "- 180",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 119.0, 35.0, 17.0 ],
																					"outlettype" : [ "int" ],
																					"id" : "obj-1",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "* 180.",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 95.0, 40.0, 17.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-2",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "/ 3.141594",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 72.0, 63.0, 17.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-3",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "newobj",
																					"text" : "+ 3.141594",
																					"fontsize" : 9.0,
																					"numinlets" : 2,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 50.0, 63.0, 17.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-4",
																					"fontname" : "Arial"
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "inlet",
																					"numinlets" : 0,
																					"numoutlets" : 1,
																					"patching_rect" : [ 50.0, 30.0, 15.0, 15.0 ],
																					"outlettype" : [ "float" ],
																					"id" : "obj-5",
																					"comment" : ""
																				}

																			}
, 																			{
																				"box" : 																				{
																					"maxclass" : "outlet",
																					"numinlets" : 1,
																					"numoutlets" : 0,
																					"patching_rect" : [ 50.0, 141.0, 15.0, 15.0 ],
																					"id" : "obj-6",
																					"comment" : ""
																				}

																			}
 ],
																		"lines" : [ 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-1", 0 ],
																					"destination" : [ "obj-6", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-2", 0 ],
																					"destination" : [ "obj-1", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-3", 0 ],
																					"destination" : [ "obj-2", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-4", 0 ],
																					"destination" : [ "obj-3", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
, 																			{
																				"patchline" : 																				{
																					"source" : [ "obj-5", 0 ],
																					"destination" : [ "obj-4", 0 ],
																					"hidden" : 0,
																					"midpoints" : [  ]
																				}

																			}
 ]
																	}
,
																	"saved_object_attributes" : 																	{
																		"fontface" : 0,
																		"fontsize" : 12.0,
																		"default_fontface" : 0,
																		"default_fontname" : "Arial",
																		"fontname" : "Arial",
																		"default_fontsize" : 12.0,
																		"globalpatchername" : ""
																	}

																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "clear",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 874.0, 351.0, 33.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-2",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "t clear",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 851.0, 71.0, 40.0, 17.0 ],
																	"outlettype" : [ "clear" ],
																	"id" : "obj-3",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "t b",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 485.0, 290.0, 20.0, 17.0 ],
																	"outlettype" : [ "bang" ],
																	"id" : "obj-4",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "linesegment 70 0 70 140 246, linesegment 0 70 140 70 246, pensize 10 10",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 485.0, 316.0, 355.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-5",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "pensize 1 1",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 190.0, 340.0, 60.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-6",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "t b",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 190.0, 204.0, 20.0, 17.0 ],
																	"outlettype" : [ "bang" ],
																	"id" : "obj-7",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "pensize 2 2",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 190.0, 320.0, 60.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-8",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend paintarc",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 643.0, 342.0, 84.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-9",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "$1 $2 $3 $4 $5 $6 7",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 643.0, 292.0, 105.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-10",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "$1 $2 70 $2 248, $1 $2 $1 70 248",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 260.0, 203.0, 173.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-11",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "message",
																	"text" : "pensize 1 1",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 413.0, 342.0, 60.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-12",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 0 0 90 360 28",
																	"fontsize" : 9.0,
																	"numinlets" : 7,
																	"numoutlets" : 1,
																	"patching_rect" : [ 485.0, 251.0, 115.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-13",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend framearc",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 550.0, 342.0, 89.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-14",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 413.0, 99.0, 30.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-15",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 99.0, 29.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-16",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "cartopol",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 2,
																	"patching_rect" : [ 378.0, 121.0, 45.0, 17.0 ],
																	"outlettype" : [ "float", "float" ],
																	"id" : "obj-17",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 483.0, 154.0, 31.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-18",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 448.0, 154.0, 30.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-19",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "!- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 413.0, 154.0, 33.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-20",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "!- 70",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 154.0, 32.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-21",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 0 0 255",
																	"fontsize" : 9.0,
																	"numinlets" : 5,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 176.0, 153.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-22",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend frameoval",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 378.0, 251.0, 93.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-23",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 70 70 212",
																	"fontsize" : 9.0,
																	"numinlets" : 5,
																	"numoutlets" : 1,
																	"patching_rect" : [ 251.0, 154.0, 99.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-24",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend linesegment",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 251.0, 251.0, 100.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-25",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 138.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-26",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "+ 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 109.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-27",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 80.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-28",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "- 3",
																	"fontsize" : 9.0,
																	"numinlets" : 2,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 154.0, 27.0, 17.0 ],
																	"outlettype" : [ "int" ],
																	"id" : "obj-29",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "pack 0 0 0 0 255",
																	"fontsize" : 9.0,
																	"numinlets" : 5,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 185.0, 127.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-30",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "prepend paintoval",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 251.0, 88.0, 17.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-31",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "newobj",
																	"text" : "unpack",
																	"fontsize" : 9.0,
																	"numinlets" : 1,
																	"numoutlets" : 2,
																	"patching_rect" : [ 51.0, 65.0, 39.0, 17.0 ],
																	"outlettype" : [ "int", "int" ],
																	"id" : "obj-32",
																	"fontname" : "Arial"
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "outlet",
																	"numinlets" : 1,
																	"numoutlets" : 0,
																	"patching_rect" : [ 51.0, 395.0, 15.0, 15.0 ],
																	"id" : "obj-33",
																	"comment" : ""
																}

															}
, 															{
																"box" : 																{
																	"maxclass" : "inlet",
																	"numinlets" : 0,
																	"numoutlets" : 1,
																	"patching_rect" : [ 51.0, 31.0, 15.0, 15.0 ],
																	"outlettype" : [ "" ],
																	"id" : "obj-34",
																	"comment" : ""
																}

															}
 ],
														"lines" : [ 															{
																"patchline" : 																{
																	"source" : [ "obj-34", 0 ],
																	"destination" : [ "obj-3", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 60.0, 54.0, 860.5, 54.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-10", 0 ],
																	"destination" : [ "obj-9", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-13", 0 ],
																	"destination" : [ "obj-10", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-1", 0 ],
																	"destination" : [ "obj-13", 5 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 1 ],
																	"destination" : [ "obj-1", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-13", 0 ],
																	"destination" : [ "obj-14", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-18", 0 ],
																	"destination" : [ "obj-13", 3 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-19", 0 ],
																	"destination" : [ "obj-13", 2 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-20", 0 ],
																	"destination" : [ "obj-13", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-4", 0 ],
																	"destination" : [ "obj-5", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-13", 0 ],
																	"destination" : [ "obj-4", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-21", 0 ],
																	"destination" : [ "obj-13", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-18", 0 ],
																	"destination" : [ "obj-22", 3 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-18", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 146.0, 492.5, 146.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-19", 0 ],
																	"destination" : [ "obj-22", 2 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-19", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 146.0, 457.5, 146.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-4", 0 ],
																	"destination" : [ "obj-12", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 494.5, 311.0, 422.5, 311.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-20", 0 ],
																	"destination" : [ "obj-22", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-20", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 146.0, 422.5, 146.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-15", 0 ],
																	"destination" : [ "obj-17", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-15", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-22", 0 ],
																	"destination" : [ "obj-23", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-21", 0 ],
																	"destination" : [ "obj-22", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-17", 0 ],
																	"destination" : [ "obj-21", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-16", 0 ],
																	"destination" : [ "obj-17", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-16", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-24", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-24", 0 ],
																	"destination" : [ "obj-11", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 260.5, 185.0, 269.5, 185.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-11", 0 ],
																	"destination" : [ "obj-25", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 269.5, 236.0, 260.5, 236.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-24", 0 ],
																	"destination" : [ "obj-25", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-24", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-7", 0 ],
																	"destination" : [ "obj-6", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-7", 0 ],
																	"destination" : [ "obj-8", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-24", 0 ],
																	"destination" : [ "obj-7", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 260.5, 177.0, 199.5, 177.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-26", 0 ],
																	"destination" : [ "obj-30", 3 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-26", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-27", 0 ],
																	"destination" : [ "obj-30", 2 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-27", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-28", 0 ],
																	"destination" : [ "obj-30", 1 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 1 ],
																	"destination" : [ "obj-28", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-31", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-25", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 260.5, 371.0, 60.0, 371.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-23", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 387.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-14", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 559.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-12", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 422.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-9", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 652.5, 371.0, 60.0, 371.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-6", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 199.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-8", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 199.5, 372.0, 60.0, 372.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-5", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 494.5, 371.0, 60.0, 371.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-3", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 860.5, 386.0, 60.0, 386.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-2", 0 ],
																	"destination" : [ "obj-33", 0 ],
																	"hidden" : 0,
																	"midpoints" : [ 883.5, 386.0, 60.0, 386.0 ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-30", 0 ],
																	"destination" : [ "obj-31", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-29", 0 ],
																	"destination" : [ "obj-30", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-32", 0 ],
																	"destination" : [ "obj-29", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
, 															{
																"patchline" : 																{
																	"source" : [ "obj-34", 0 ],
																	"destination" : [ "obj-32", 0 ],
																	"hidden" : 0,
																	"midpoints" : [  ]
																}

															}
 ]
													}
,
													"saved_object_attributes" : 													{
														"fontface" : 0,
														"fontsize" : 12.0,
														"default_fontface" : 0,
														"default_fontname" : "Arial",
														"fontname" : "Arial",
														"default_fontsize" : 12.0,
														"globalpatchername" : ""
													}

												}

											}
, 											{
												"box" : 												{
													"maxclass" : "inlet",
													"numinlets" : 0,
													"numoutlets" : 1,
													"patching_rect" : [ 46.0, 50.0, 15.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-2",
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 219.0, 308.0, 15.0, 15.0 ],
													"id" : "obj-3",
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "outlet",
													"numinlets" : 1,
													"numoutlets" : 0,
													"patching_rect" : [ 172.0, 308.0, 15.0, 15.0 ],
													"id" : "obj-4",
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "0 0",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"numoutlets" : 1,
													"patching_rect" : [ 219.0, 81.0, 23.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-5",
													"fontname" : "Arial"
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "70 70",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"numoutlets" : 1,
													"patching_rect" : [ 129.0, 81.0, 37.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-6",
													"fontname" : "Arial"
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "newobj",
													"text" : "loadbang",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"numoutlets" : 1,
													"patching_rect" : [ 129.0, 50.0, 48.0, 17.0 ],
													"outlettype" : [ "bang" ],
													"id" : "obj-7",
													"fontname" : "Arial"
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "local 0",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"numoutlets" : 1,
													"patching_rect" : [ 172.0, 81.0, 39.0, 15.0 ],
													"outlettype" : [ "" ],
													"id" : "obj-8",
													"fontname" : "Arial"
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"source" : [ "obj-5", 0 ],
													"destination" : [ "obj-3", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-7", 0 ],
													"destination" : [ "obj-5", 0 ],
													"hidden" : 0,
													"midpoints" : [ 138.5, 73.0, 228.5, 73.0 ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-8", 0 ],
													"destination" : [ "obj-4", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-1", 0 ],
													"destination" : [ "obj-4", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-7", 0 ],
													"destination" : [ "obj-8", 0 ],
													"hidden" : 0,
													"midpoints" : [ 138.5, 73.0, 181.5, 73.0 ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-7", 0 ],
													"destination" : [ "obj-6", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-2", 0 ],
													"destination" : [ "obj-1", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-6", 0 ],
													"destination" : [ "obj-1", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
 ]
									}
,
									"saved_object_attributes" : 									{
										"fontface" : 0,
										"fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"fontname" : "Arial",
										"default_fontsize" : 12.0,
										"globalpatchername" : ""
									}

								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "pak 0 0",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"numoutlets" : 1,
									"patching_rect" : [ 50.0, 94.0, 120.0, 17.0 ],
									"outlettype" : [ "" ],
									"id" : "obj-2",
									"fontname" : "Arial"
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "scale -1. 1. 140 0",
									"fontsize" : 9.0,
									"numinlets" : 6,
									"numoutlets" : 1,
									"patching_rect" : [ 160.0, 66.0, 92.0, 17.0 ],
									"outlettype" : [ "" ],
									"id" : "obj-3",
									"fontname" : "Arial"
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "scale -1. 1. 0 140",
									"fontsize" : 9.0,
									"numinlets" : 6,
									"numoutlets" : 1,
									"patching_rect" : [ 50.0, 66.0, 92.0, 17.0 ],
									"outlettype" : [ "" ],
									"id" : "obj-4",
									"fontname" : "Arial"
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"patching_rect" : [ 160.0, 30.0, 15.0, 15.0 ],
									"outlettype" : [ "float" ],
									"id" : "obj-5",
									"comment" : ""
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"patching_rect" : [ 50.0, 30.0, 15.0, 15.0 ],
									"outlettype" : [ "float" ],
									"id" : "obj-6",
									"comment" : ""
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 159.0, 15.0, 15.0 ],
									"id" : "obj-7",
									"comment" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"source" : [ "obj-3", 0 ],
									"destination" : [ "obj-2", 1 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-5", 0 ],
									"destination" : [ "obj-3", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-1", 0 ],
									"destination" : [ "obj-7", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-2", 0 ],
									"destination" : [ "obj-1", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-4", 0 ],
									"destination" : [ "obj-2", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-6", 0 ],
									"destination" : [ "obj-4", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
 ]
					}
,
					"saved_object_attributes" : 					{
						"fontface" : 0,
						"fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"fontname" : "Arial",
						"default_fontsize" : 12.0,
						"globalpatchername" : ""
					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "lcd",
					"numinlets" : 1,
					"numoutlets" : 4,
					"patching_rect" : [ 347.0, 259.0, 140.0, 140.0 ],
					"outlettype" : [ "list", "list", "int", "" ],
					"id" : "obj-17",
					"local" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "unpack 0. 0. 0.",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 3,
					"patching_rect" : [ 218.0, 505.0, 101.0, 17.0 ],
					"outlettype" : [ "float", "float", "float" ],
					"id" : "obj-18",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"patching_rect" : [ 308.0, 537.0, 35.0, 17.0 ],
					"outlettype" : [ "float", "bang" ],
					"id" : "obj-19",
					"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
					"fontname" : "Arial",
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"patching_rect" : [ 218.0, 537.0, 35.0, 17.0 ],
					"outlettype" : [ "float", "bang" ],
					"id" : "obj-20",
					"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
					"fontname" : "Arial",
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
					"patching_rect" : [ 263.0, 537.0, 35.0, 17.0 ],
					"outlettype" : [ "float", "bang" ],
					"id" : "obj-21",
					"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
					"fontname" : "Arial",
					"triscale" : 0.9
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "distance > 0",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 408.0, 200.0, 63.0, 17.0 ],
					"id" : "obj-22",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "inclination",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 314.0, 200.0, 54.0, 17.0 ],
					"id" : "obj-23",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "pak 0. 0. 0.",
					"fontsize" : 9.0,
					"numinlets" : 3,
					"numoutlets" : 1,
					"patching_rect" : [ 218.0, 453.0, 195.0, 17.0 ],
					"outlettype" : [ "" ],
					"id" : "obj-24",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "flonum",
					"fontsize" : 18.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"hbgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"patching_rect" : [ 218.0, 215.0, 61.0, 27.0 ],
					"outlettype" : [ "float", "bang" ],
					"id" : "obj-25",
					"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
					"fontname" : "Arial",
					"bordercolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"triscale" : 0.9,
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "declination",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 224.0, 200.0, 57.0, 17.0 ],
					"id" : "obj-26",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "ahrs_aed2xyz",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 1,
					"patching_rect" : [ 218.0, 477.0, 74.0, 17.0 ],
					"outlettype" : [ "list" ],
					"id" : "obj-27",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "<--Click here to calculate magnetic field strenth in your location",
					"linecount" : 3,
					"fontsize" : 9.0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 369.0, 145.0, 138.0, 38.0 ],
					"id" : "obj-28",
					"fontname" : "Arial"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 198.0, 138.0, 162.0, 52.0 ],
					"id" : "obj-29",
					"embed" : 1,
					"data" : [ 14417, "png", "IBkSG0fBZn....PCIgDQRA..A7K...fMHX....vUaeo0....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wY68kGeTUc1+eu2YlLKYYRHDRBgLIDVEHfBnPg.tg.RDoPsVYw1e9V0Bt7ZWMB1p1VJswe012e1WknB0p.ATYqBAQTDPhBHPYIPHKPBYBYmIYljLyjY6d98G24bx8NKYuVWte4yPl64dtmyy4buy4497bdV3HDBAJPAJPAJPAeKB7+ml.TfBTfBTfB9xF8IleD++Og9+DhXgDRmkSKy+w9qfjOxaMEn.En.Enfurf59xEwAQVVbD+Gvw4mEFG37yLT7aTFaD14oGCHVmNKSAJPAJPAJ3KGv021yO+R0wwXCJ4uc+0R7W+NYExovBTAJPAJPAeog9jje.b.b.s5zChQuFT70rgqzP63bU0BZ2kWLhgDEZvVGPkJNLr3iDc3xKtyLSBp34PZC1feFcbR3aR7yHUAJPAJPAJ3e+nOI4mKO9v6epZv0Z1ItiLSDuxGVF..t8wODbhxrfOu7qiTiORL+IkDNYEMCKs4FwFoZTjYaXUycTHqQm.FRLQ.iQp0eK1SkZTAJPAJPAJn+iPv7iZdJbfiP.gCLkRVsE63rW0JNxkZDe5kZD26TRASc3wA0pUgAEsVLz3zAOdEPy18fAEoFzrcOHZcpwNNQ0PMOOl9nGD9q6qLDgJN3zs.dfuiIL8QGORM9HgbCega.icHc3w80TIKIDxWoo8.me+pN8p.En.E.D.yuNY6APHbLMQ5xiOvyygW6iJCu8QqBiN4nwjRKNj4vhASYDCFs0gGbjK0HJs1VQCVchq2la..L3ni.wGsNLoziCybzCFIFqNztSO3MN3UvUZnMLr3iD01hC73yaLXxoGWmDAGkj5+Kh9k4hwcUe0aYBSaqti9+pJy8upRWJPAJPA.AJ4Gwuwnv0orWMXyIdiCdEL6aXH35s4B1b5AyYBIB.f8e15va7IWAEesVg8NbCzgO.AB.OGfJN.e9aZcpggHTgwaJVrzYjNt0wODnhmCeRQMfO3b0B.fkNyzvBtwjQhF062jX54KZ9Iexmf1ZqMYkkUVYg3iOd+CqdOCvhJpHTQEUvNN8zSGSZRSJj0Mv12tc63Lm4LnjRJAm4LmA0We8nt5pC..QGczH8zSGYjQF3FuwaDSXBS.ojRJA0d.nWy7q7xKGEWbwPPP.777PPP.yctyEFLXX.mIjz4GAAAnVsZrfEr.nRkpAz94+zvrYyvrYyvnQiHyLy7+zjSOB1rYCEUTQ..HyLyDFMZrO2VeUd7WXgEBfd+XrnhJB1rY6qjiIE7kHHRf.6+EHBBBDyWucxiswSR9Qu5wIuxGVJo1lsSr4vM42uihHI+H6ff68sH36sIBt+sH94Ax2+e2Jgao4S3df78W1lE+6R1DA26aQR9Q1A4W+Nmizgauj2+TWirjW5njW78Kl7QmuNFkHHHP5ovjISA5Dgjm3Idhd70GJ7POzCIq8dvG7ACY8jRm0TSMjm+4edxTm5TI777AQSg5SFYjA4QezGkbpScpfZydybfc61ISbhSLn1+Mdi2HjzZ+A0TSMjDRHgf5q8u+82m6G50r7kubRZokFYhSbhDqVsJqNzyEHN+4OOIszRijVZoQV8pWsryQKOTed0W8UIG8nGkjVZoQV9xWdPs4rl0rjU+INwIR1xV1hr58pu5qxN+QO5QC44BrbBgPl3DmHIszRiLqYMqfNW3notCG8nGkMGI8y8bO2C47m+7xZ6P8o+N9oedrG6wX8Ggz48svM+7pu5qx9dnlOV8pWMqdDBgrksrE17mz9jhPMdn8WucLE345pwiB95EBvZO6Tcis0gG75G7J3jWwBtwziC+fokJNYEMie4VNKtX4V.TyCXPiLmbf.B.Ouj1wum+wqF.BfKBU.QnF00rCr1sbVrqunZ7+7ilBV828Ff4q6.CJZs3MORE3AmcFPcuPPkXhIlfJaiabiXEqXEXZSaZ87FRBhLxHkcrd85CY8333fGOdva7FuAV6ZWKSBudJpnhJvq+5uNdy27MwC8POD9c+teGRLwD60z6G8QeDN+4OePkme94iG9geXlDhCDXG6XGnolZB.cJcJ.v67NuCl27lW+VJSiFMBa1rg7yOerpUspts96ae6i887yOejSN4vjDHmbxA..UWc0H+7yGlLYBKcoKE.Hru0eQEUDVwJVArYyFxJqrvLm4LYW+ZVyZ..vxV1xBhlyKu7PVYkU2RuETPAvlMa.PTxpBJn.jc1Y2sWWWAozlISlP1YmMhIlXvEtvEPAET.LYxjr5KcdHPzWF+z58Ye1mgBJn.TXgEh8rm8DT+FNrpUsJr0stUX1rYje94yZe5wlLYBqZUqBEUTQxngTSMUTc0UylOCGVyZVCxO+7A.P1YmMlvDl.Zs0VC4bizmm15V2ZPiUE7MCvX9I0Izawta7JeXY3DW1Bdj6XD3dmZJ3+4CKG4tyK.OdD.LDA6pHRLMENvCBDDUYIoSWaGP..p.ffnAznQEHZTiKd0VvBV2gv+2e3jwSc2iA+e2ykvGUT8..X9SLIjbbF.H9.giGf4MDgegUoKD6zoS7zO8SiO3C9.XvfgAnoqfQKszBdrG6wv1111BachN5ngNc5..fWudgMa1ffffr53wiG75u9qiie7ii+9e+uioLkozsLQHR1WvcricDx5b1ydVTbwEiwMtw0KGYgtuDDDv68dumrxo3HG4HngFZnOw7NvwpISlPd4kGxN6r61EPKnfBfQiFwxV1xv5W+5QAET.aAKJyyBKrPYKhRAU0YRQd4kGrYyFV1xVFV25VGq7ILgIf0rl0fbyM2fVPzjISnvBKrGwH6y9rOC.hLlyM2bwm8YeV+h4mYylQt4lK.DGuTF9Trt0stfTKXfyCRQeY7OyYNSrpUsJrpUsJ73O9iiBJn.TPAEzid4EJV4JWIVyZVCxKu7Xsed4kG6b.cd+JTiyvgBJn.je94CiFMhMu4MK6kdxImbjw3zrYynnhJBYmc1rua1r4dLSbE70GvSHznshngtXytKbnK1.LawAl1HiGeuokJdl7OKV6VNK7PH.Qn1O6GBfffHeM+6RGw+wD.PDHhmSv+hiB9DKyuEjBP.mFUviWA7S2vIwZ24EvO91y.iJonwEq1FNV4VD6C+L9P2v3CP9Bwbbb3S+zOEabiab.cBSJrXwBxN6rYL9jt.dxImLdzG8Qwl27lwgNzgvIO4IwoO8owQNxQv1291wy+7OOlvDlfL5E.37m+7XgKbg3Dm3Dca+SulpqtZr+8u+PVmVZoE7AevGvNt2JAHgPjcMm7jmDm5TmJjzREUTA97O+y6UsenfMa1vRW5RgMa1XK9ENTPAE.ylMiryNarfEr..H+M26K8cAET..PPKttrksLjYlYJqNTP6aJSntp8kxD1nQixjDru.50mUVYERFB8l8CquN9kBoOW2avxV1xPVYkESZO5eyJqrBhYa94meHewkPA5yC4jSNgTZeoyOacqaE.hLyo2S6pwpB95K3437y5ivAAAB1xmUE1RgUAMp3wOaAiAOyVOG9G6ubQUbpRkrfVFmJ+gFTuD.u9DMxE..uB.BDvwyKx7yqe4J44.7J.NB.Owe6nlCPMO9Ma9L3UOP4XEyJMTuUmHuO9xnZKN..m+E4oJXsmA5B14latnxJqb.XpRd65wiG7DOwSficriI6b50qG+pe0uBG6XGCu1q8ZX4Ke4XJSYJHszRColZpHyLyDKdwKFuvK7B3vG9v3ke4WFImbxxLrk5pqN7C+g+PTc0U2inoCbfCHSMjTUzRaussssAWtbIqrdJ333Xe..1yd1Cb5zI..344gFMZjMur4Mu4dU6GNrpUsJXxjI1hfgCTonVvBV.xLyLYRf0UWSWApghjUVYERlFzEDCr8yLyLwxV1xXKXGNPWHkpxwryN6tkYR2AoyATX1rYTXgEx9DH8Z1rYr90ud1GJM2WG+TXylMFyl.u98su8IqOozsTPkvKu7xKHo9.DYPZxjIXylMrhUrBrhUrBFMGNPma6IRWS0hP1YmMq9TFhJ3aVfWR34DU2rC..DajpwO36XBGt3Fwar2RDYpwQie0b.d8gkOyzv+7WLariewrvrlPh3Ql6nv6+KlMd6mbFX7oEKV2JtQ7O+kyB+9kNIL9zhEu8SNC7O+kyFOU1iEDWdYBDxAQFh7QnBm4pVwjSeP3NyLIb4FZGu9AuBb4wGiX6KaaUM0TCd9m+46+yT9AkIve+u+2CRUmwEWbXqacq3EewWDokVZAcsAJEU7wGOdxm7IwQNxQvTm5TkctxJqL7zO8SCOd7zszzl1zlXeWmNc3oe5mFIjPBr16Tm5T3BW3B8tAZHfCGNjsPPhIlHVyZVir8C8PG5Pn1ZqsW21AN2.HegvPAJSCSlLw1qM5BV+m3s06N5EnSoPnz4Lm4LA.BIifdKjpZtBJn.FygUrhUDz7AUUozO8GokADYPrhUrBL6YOaTTQEwdY.oH+7yWVeFJI2nR4Icu+jtOpFMZD6YO6gUVgEVHV3BWXW9BGRu1tBzWRH6ryFFMZDlLYBYlYlL0epfuYAdP3.H9fff.Nv4pCmtxlQVicHX12vPvbxLI7hO7MCCQFAfaefmH..BfOALkgGORNNcH43zgGLqzwCcaCGG4RMhzh2.dj6bj3txLI729vxvruggfezslAhLBUX2mpZ7CusLDaO3WVNGdv3Gdbnve+cgs8Ty.Z0nBqHqzwsN1Aic8EUi7+7p7Sp8L2UPud8X1yd1PiFMr5uksrE7O+m+yArIsZqsV7a9M+Fwwf+9H5niFacqaEKZQKhUu.WLmJAUfkOpQMJryctSYtRAGGG1111VXUmIEm9zmFm7jmjcbpolJd1m8YwXFyXjUu+w+3ezyGfgAezG8Qxb+iwN1whm8YeVLjgLDVYszRKgc+G6JDp6sz2xObR+QU2WlYlISBGpwO0eea8voFxVas0vdMlLYpKk9iJMlISlXemtfLU8s8EPaCoWe1YmM17l2bXMVirxJKb0qdU1m.kXuuL9ADk.NmbxIjZ.Xyadyx5yvsmcRkzS52oft2cR2+tbyM2vRygZ9ITf9B.olZprmmnsuhzeeyC7hZTTENwUrf16vGTwyy7iuXzqA+pEdCXSO4LfA8ZffO+V0t+Ep9hKaAGurlgZ07viWhembuCVi+8mVpPiZN3xiOLhjhFe2olJDDDDUGJ.Ht7hwOp3w6+z2J9NiJAnQEO.g.Mp3wOM6a.iK0XQMVb.6t8FPVhH7vkKW3m7S9IXdyadLlLBBB34e9mm8ii9qkO929a+MlZFkpd04Mu4IqdgiYcnJO0TSEabiaDIjPBxZ2+3e7OBe97EV5dG6XGL0PB.rjkrDnQiFL+4OeY06fG7f8q8UB.AIo62+6+8gFMZBZbu28tWlZV6wfD54E5BjgRZJpzRRkxgtma802VmJQA0PGBDTInBmUctxUtRlkeFtq0rYyxjJKvy2aAcO1jJ8FUZ3TSM0dUa0WG+KcoKkwPhtWl8UHUB1txPSxJqrvl27lYpAMb2uozZ2M+ROet4lK6dC8kXT12uu4AV97aXCx.znhfrFU7XHwnCzzzG.AK4VRE+O+elhHmRAvr6D8ZUCsZ3fZU7PiZNLDiZQz5TAMp3.ueFbZ0nFZ0H53yVc5AF062RQ8J.CQFAdiexsfLFRTrrCH33vwaxEFThFwPMpCGnn5QUMZG.cm4tHBAAAnWud7W9K+EDczQyJ+bm6b3u9W+qhsijPwUuEUWc0xTyHf3OtdzG8Q60sUfXJSYJ3ge3GVVYm3DmfsnVfRMZylMr6cua1wZznA2y8bO..3du26kcO..3RW5R3vG9v8YZqxJqDG7fGjcbzQGMiA6RVxRXzG.vQO5QQIkTRuqCByM2ryNajUVYEjzeTWDHyLyjsnK8CUZm95aqSu9G+webYuvvZVyZXV9W3bSBSlLgUtxUBylMGT+SONP5kxfu+PuFMZDEVXgXMqYM86Wxo+L9+2M5p8yMbLJo6uZt4lKV+5WuryQch+7yOelEtF38GpQ9zSTspB95CTS.faO9fNMpwBl7vPjZUAsZTIlnEPmQarG4NGILawAV61NG.GGZ1tK3UP.dEDPcV6.02hS7v2wH..vAOTkH03iDezEZ.wGsVzpSO37lshCew5QxFEM4e31GV8xuQ7cFknjNb9SQRM1gOr3iXAOclFwclYRnMWdvaezJwZ9tiGwnWSOZP0QGcfQMpQgbxIG7q+0+ZV4u3K9hXgKbgXJSYJh8Yevez9zO8SQM0Tirxdlm4YFvhrIO0S8TXCaXCnolZh4ZA6ZW6BKbgKLHZtvBKDW5RWhc7Tm5TwTm5TA.vnG8nQVYkE9zO8SYmee6aexTKauARMpF.HyEDl7jmLF8nGMJqLw.btSmNw92+9CaDwITPpA+DHV4JWIJrvBksPL8MwWvBVPPRgXznQje94iBJn.YlpeOE4jSNnnhJBEUTQX1yd1r88gFoSdkW4U5xqeYKaYL2EfBpjTTl4RQVYkExKu7BRZUpQoHEgx0AnpAjJoB0BI6JogBUaSum1eG++6DTePzjISvjISrn0BUE4gBYkUVXcqacL2zHu7xiwPqnhJB4jSNr8DeoKcoAwXmNW7Ye1mISMx6ae6S17alYlYOxOOUvWMfZN.zhC23OumRfS29vKbeh2347afKRWN52e+SDkVWa389jqf+vttH.yoGH.BDXHBUvkWA3SffccxpA.G19wqtSe+C.+8iTIfKeX3YLH7SlynnWM33.73S.q7Krh5s5EauJ63UGajX3IDE..r2gmdLyOpzQO4S9j38du2Cm6bmC.hKJul0rF79u+6CsZ01UMQXwG9genrioLYj128l32IfbFZIlXh3dtm6Au4a9lry+IexmvBGSRQf6qR1YmMaboUqVL24NWYL+10t1EV25VGKru0afz9hmmGKbgKjwvOgDR.yblyjw7iV+e9O+myrFztCc0bVVYkExJqrjYfDAZ3HRA0pO6qNPNkYB0nLn6MW1YmMV4JWY2J0iQiFwJW4Jk41CT5kZfKAhryNaje94i8su8wpiT+2ihv42bYlYl3S+zOE4kWdLZFnS0eF3hxgpsoya82w++NQlYlIxN6rYzEUR6tyeBoLG2291GyI7oiISlLgbyM2vJQa1YmMxM2bC5koBTRvbxIGEleecBDBgbjhafbW+gOg7A+qqQdupcRV7QtNonVbQHDBQv++HBdIDBgTay1Ii+WrWwPUFM7k8.a0+G+g3L1eC779C+YeuMQd8OtbV6SCrZO24rQv+vLAuc0DC4WCojV8P9f+00H+hM8uHGo3FBaXpYBSXBxByVRCIQ6e+6WVnFiiiSVH+Jb3IdhmPVa9XO1iQDDDBJTp0eCiZgJTfsyctSBGGmr943G+3xp+Uu5UIImbxryqWudRIkThr14e8u9WjniNZYsya8VuUulF+hu3KH50qm0FojRJjqe8qKid16d2qLZVud8jCdvC1q6KEn.EnfuL.O.fiNDMm930qFmnI2XWkYGK5PMii2jK32ezA.OHffjiy.d0e7MCUQnRzG93.3IBhtKA6e7fCh9xmXCH.dhf3wBDL7TLhkMqzkvBlC4eUG3Ob1NCN0N8JfSawMhWuZDqAM3BUasOwbedyadx1GMBgfm64dNzPCMzqZGdddzXiMJS0e.Plp8H8g8PLTR7LxQNRDarwJqLp5Un0e+6e+xBkZ24cdmXzidzxtla5ltofr5yssssETzko6v68duGb5zIquWzhVDS5QZY21scaXnCcnrqwoSmXW6ZW8p9oW3FmJPAJPA8KvSHD7wWrQLjXzhQLrXwUZyCfJfJZ0CVdgsfKX0ingVHIu9M6aXH3Wdu2.PGdEMLF1B3RWHmSLrjAwvdl.GGD33.7IfUMmQhHiPs+ywgCVeG3QNlU3iH.nhGfmCDuB3BV8fLRNFTm0NPys6tOOHetm64j42c0UWc34dtmSVcHgvOyjBMZzfRJoDzQGcZMq50qGYjQFcNhGfxbBlLYBIkTRxJq3hKl8cOd7f24cdGYmeQKZQx5e5X49tu6SV8N0oNEJu7xCpdgCVrXA6cu6kUWdddlAtHEQFYjr8SjRG6ae6CVrXoG2WecNeFK0oxUfBTvW8AuCO9fWuBvsOBr5gfF5vGKkDUgU23m7EVgKO9XtZ.0cCV88dCXVSdn.N7D9WXmi.3y+UvA.Wdw3yXP3mLmQxpxAquCbeGoY3vsOwHMJm.37KYhUO.02hcX951wjRKtd8fitXaJojB9C+g+frysoMsIY6emznXRnfZ0pQUUUkrEviJpn50lRdOAwDSLAI4mTo7Jt3hwQNxQXGmbxIi65ttKY0mNVl+7mur8cqolZBG3.GHn5ENFSG+3GWlQ0LgILAlQ0PA8ZoQ+C5wUTQExXFzSd4fUrhUfzSOcYeVwJVAV+5WeXshw0u90ypqz8govBKLn1R5GozV3ZitCTmrd1yd1LyiO8zSmseZRa2.GSTDpxjdNZ4c0XY8qe8c43sqleW3BWnho7qfu0A0MXsCL5gFMFbLZgUaNQaz.JBA.pUgOulNvROlUrooajIsFADXLRsHue7Mi4TyAQcW2NfNMAq1JBmnyTH3C.bPUDpwebo2neCWgfCVuKbeGoYXsC+L9H9kszexrsdmdQLQqGiYnwfJZr8d8fS5hsOvC7.Xm6bmXm6bm.PTsb+5e8uFyXFyf4RDjPX.JRQfK9pVs5fx9CCDfiiiEHroPpZZ2xV1hLUWdq25sFxHJCfnin+c9NeGYF9x1291wi+3ON344YFnSfYhc5eCkQ0H0vaHRLvmoO8oKypOA.d228c6SVXpzH1O0.E1291G17l2bHCaVTHMJ7SsbQ.QeBrvBKjk8Anmu6ZitBRyxAYlYlxhsnAloQj1u.AGsQntp.kFCE5oYnhtJiMPAc9kl0Gd7G+wgQiFULXCE7sFnNwX0gyVkU3wqOnMRs.vO2OdNvQD.QCO10UriGDfw.jln0G2vLhs8yxBOvesPTWCsCNV1dPB3fXZNxgGr5keiXgSQLwstwJbhe4IsJx3im2ea52xOI7.Pzwta1tGjnQcXHF6aVmIf3BzpToB+9e+uGG5PGBszRK.PTEfu7K+x3Ye1mUjTG.TaIoWXsm8kq2lMaxBT0.hL1CW6nUqVb629sKi42IO4IwYNyYXt7gTHkwW0UWsLe6immGeuu22Kr8U7wGePV84gNzgP0UWMRM0T6UyMRcggbxIGVLbLu7xSVjAoqhB+gJCNPy9.RQeIR9K0ZIW25VmLlkqZUqJnWTJT8qTPMa+byM2vx.pmlgJ5pL1.ERmeotFRQEUjByOE7sFv2lSO3lyXPXtSJYDiNMHZlFxD.A9SkPZTgccE6XtGtYbAqtE8KA+VByruggf2+YtUL9QEOHNbChWeLUiR.Avi.fCO3wVz3vu+9mHptMO3GerlwCeTKhL9TAw3FJ8p3HLI+hSCGRPuJ3UP.mqpduAuDnp7F23FWPgToW5kdIbwKdwdT6EXN8yqWuAEISFn12u.iomwEmnZeKrvBkQuYjQFXNyYNAc8RoiEsnEIi1c5zIKDjEJ5kV1t28tkYfOyXFyHHFlApt3Eu3EK670UWcL0K2Wmaj5aYApRxAhnveeoMngVsksrkERoD6sQ3DpY2WTQE8ktyT2ehFKJPAecE7Q6224tR8sCONcgD042JNEDcwcNp4dplGedctvr9vqi0bVanwNDUkI.vTyHdbveycfGaQiCIGmd.O9.ocW.t7hHMDA90K+FwpVxDwu5Lshwt2lve+hsCvCvohyuTdhgWawtxeTLwGvfzoF0XwANyUshwLzfSXscGnK1JcQ2e5O8mJaOqZokVvy7LOCSMhgauu750KF9vGtr1p81aGW6ZWqWSWcGMa2tcX0pbl8T0ZticrCYz3blybPGczAZngFj8oolZh88gNzgFzdSVPAEzkwoQWtbEj0ZN+4OeXwhkf5GZe0TSMgQLhQvXTSwN24N60VXZff5CV1rYS1d0MPDE96KsAMzpEN+1KT0WZFMHTFFSWEJ25MHbYrAonnhJBEVXgryGp.QsBTv2jg5HiPMTqlGGnn5w+0rRCIouyj6NGw+11QHfvC.ddXsCe3OdFa3MurcrhQDElexQfrhWCRznd7J+WSEO6BGC18YpCls3.wGkVj8MMTDcBwfMTZan4N7hm5FhDGoQW3yqyEHpHfGb9Y8IlnaE8RLd.sBXrFUiyUdK3AyJcTsE6CHCXsZ0h+7e9Oi65ttK3winkrVPAEfst0shku7kGVoS73wCxHiLfNc5XwRSmNchJqrRLqYMqADZihZpoFYVIIfn6OzZqsJKblAH5FBe7G+wvqWugs8333PiM1H66DBAm+7mGm7jmD24cdmg7ZJojRXFUC8ZdkW4Uva7FuQXeAAZ8Zuc46O6gO7gQ4kWdPtcQuEAJgBMTWQCuWFMZDYlYlrHxQOwYr6usQOUpo.sDzP4Pzz.iMMJszWYFEnCrGp7gmzySCQbJR.pfuMA0..Sd3CBmqxVvGewFwTLkrn0dxS.gHFnoE3A3Hbf.AvyyCAUDTuce3OeVq3UKQMRRGOlT7ZvHhVCFpAU39t0QgT01ISjRayKFSLpQStHnjV8hJZ0K33UABQ.DN+RbQClZDU.bBPqZdjUBQfOpLfW+fWF+kGbxCXC5a8VuU7DOwSvh0mDBA+te2uCyYNyIrYgbAAAjVZognhJJYAR5.SUP8287C.n7xKOH+PbZSaZ3ce22ksekTzRKsDTYcEjx35ce22MrL+jZTMzqQpEm1cPZbH0oSmX6ae63Ye1mseM+D39nEXT3G.LFWacqasGw7qu1F8zLE.E4jSN8nrZ9JW4JQ94murrYduEz.9b2QOlLYB4kWdLUs1ax55JPAecG7..Z34vmW90Qqc3ASaPpggHT4O.VK.NNRmpljiGBbDwuphGbpUAGt8gJr3F65J1wluR6vpKePKQbQyi2jK7iOVy319vFwxOz0w5KtUrqJcf5cJ.BuO+60G5zBOIb.PLQ39UlkMp..vE7mDQAQE5XzfAQ7gKUa6vnAMHFCpCyPnmCoK7u5UuZjQFYvJqrxJCqcsqMrWqWudAOOePR4cnCcH3vgCV6OPrmeG7fGTlZBSN4jQ5omNyRUGn1Ww8u+8GjDl.hosl9aJfJPKmc26d2vgCG8YZmZHJRsHwAhnveesMBUlTXf.TiUo6RJt8WPCSXTUsNPONTfB9pNTCPPRwpCq6AlDlTZwgDhQCxZHQfCbsNinGhKioBfHvxnQhFmhOj4f0grFhV78RUKtijzANHxzakegU7g03BN5vKfJUfSCuLOgfiHZl8zziDHh65GgW.vKA2SJZQ7QEA9o28XPat7hAEUe2ZOY8ojEdSHgDvZW6ZwxW9xYKTuwMtQbe228ga8Vu0v1FyYNygwDBPLe5c5SeZLqYMqADlR1rYKn7g27m+7YNQMPmNa9LlwLfACF5wssGOdvgNzgXGSsly6+9ueY06fG7fxbDd850ioO8o2iiSm..s0Va3XG6Xr41KdwKhhJpHLsoMsdbaPgMa1vi+3ON.5L+tIMJ7KMClCHxHiJMSWI8T+oMnAuZZlTHmbxgIMnMa1fYyl6yw.SoR+8uajUVYwrzztyBWUfB9lDTS.vfhRKlSlIAAABZytK7iFgAbfq4jwrhiP5b+3nlBOD.ApPx53wThWCZwKvKb9VwGWeG3yazMfOhn5S8mNiHAD9NDYd1IyUN5+4iGFzwg6MEcnMmtwnR1uO3QywRC.tQ.8uKcoKEu+6+9r7TmSmNwpW8pwm+4edXWneNyYNHgDRPV976EewWjIQXfR+0SjFTpTRu8a+1AoJsEsnEgCbfCf1Zqyv+1zl1zvQO5Q6ky.huwOUUsDBA6ZW6B228cexR+Q6ZW6RlTx28ce285DTaqs1JF4HGIadxoSmX26d28XlezHlO0O+rYyFxJqrXpliZvI8lnvefn+zFgKSJ..lu5IU0igJSsGN0LJMvX2W1GttJiMDJjc1Yi0u90iBJn.EUepfu0.dN+60VqN8f+1GVFNbIVvcmfZLij0B3glMFj5749iWmbhlpxAtlS7vG0B99G5532clVvmWmKQlTp36p9UF37+eDNBfOe3GMxHQLt5.O9adZrmSWCKByzeC+UgJ7es10tVVBjE.3XG6XXCaXCgUhpQMpQwLmeogwq28ce2f5i.6qtht333PkUVI9y+4+rryMoIMIb629si25sdKYkSsJwdR7DUZcBLDjUPAEHKEMUas0JynZ344wO3G7C519HPDUTQEz9I9tu661iSxs4me9H2bykYIhqacqiwLwlManfBJnKiB+zwV3hJLCDsQlYlI1yd1CV0pVELYxjLmTOPIIKrvBQt4lqrOcEnFfSeI27QM3Eoe5p8lTpy4q.E7sEvQHDB.A1c6CO+6VDFepFwxukTvQsQvb+nlf.gWbmAID+p6jyujfAja08KUHUJQNYw7yd.g.BH9HHVcpvwWvPv4uPsXaGyLRJVc3+2CMUnlK7RQIUZF.Qi0n2Xr.+s+1eC+2+2+2riyHiLvHG4HkEFvdzG8Qwq8ZuF..JszRwMcS2DKXOSHDjRJofBJnfPlC65IR+Y2tcrjkrDY8I.v67NuCF7fGLtq65tX6Cnd85w4N24vnF0n5U8A.vQNxQvce22sLi14ke4WFO4S9jgct3Tm5TL2Wn2rulu8a+13G8i9QxJa26d2XQKZQ8IIj6K0s+h9Se0Wu1urmK551fsq7ekBghlC23PZ4eY9rS2g.0DkzxBW8onuNdBWeIkVj19g55BWa0UzUnZ2PM96NZdfD7h79DPjQnF4buiCCNJs3E+fxQxdcge9DLB3ym3d8IR5R9Yf.Hvela.D.+YMHBGn5vrWEk9oSNuwLGD7ZwJr5vM9t2xvvO8tGieFex689JBU.r9QezGE29se6riqnhJBhIjTLlwLF7zO8SyZONNNTSM0fkrjkDxjGZ2ciyhEK3AdfGHn971u8aG2+8e+XyadyxL.la61tMYL95I8AEScpSEie7iWVYae6aGDBAd73Aae6aW14lyblChKt351P+Vnv7l27PJojhrxBmy02aZ2tKdj1aPW0F82ez0Wu1dpZxCWcGHlWjPM85qHT8+.KM0yVbNT08qJL9.55.LQWU+9y3o6liBULNNbysARGcEcEp1smv3qqn49K343.Hbh6KWrQpAkTaq3SKoI3vsW7GuwXvLRQGfW+p5jP76dB.zeTHvwC.Nv0o3dfSfSBSvdH7RvpFWLXFZ8f+3dKCG4RMhX0qAoODwXmIGGQTsn8SDpaBZ0pE4latAEAW5Jr5UuZL6YOa.HOPNem24chMrgMHa+4BG74yGN3AOHl+7mOKyIPQBIj.V+5WOZngFXmiR2Ke4KOn1pmp9yHiLxfRtqm4LmAkUVYn7xKGm7jmT14nRP2WX3jXhIh4N24JqrO4S9DYpYMPXwhETas0B.QCxgZIsz9t7xKmUFGGGrXwBJt3hQwEWLprxJYQFmVZoEYQmFAAAzPCMfKdwKJyMR333Ps0VKra2NHDBZngFjwn2gCGnxJqTFM5xkKTc0UKqLAAgfbjeWtbgxKubTbwEKabPoOJM6ymOV40VasnzRKEkVZoLUdRmwoAV.oO+1TSMwluZpolXVTKcrHUMy1saGW7hWDkUVYrxC0anSHDTYkUJ6ZIDBLa1Lb3vA73wCatUPPfMVJszRgEKV5xEJsYyFppppfOe9fOe9.gPP0UWM6dnEKVXyklMaF1rYCBBBnpppB1rYicuwiGOnpppBW7hWDszRKrwrGOdPCMz.N24NGprxJgff.q7JqrRTbwEixKubYiMKVrvFWlMalcezkKWnrxJi8aC58PZ4kVZonrxJCd73ABBBvmOengFZfceymOevkKWnzRKkkw4oykd73gM9oyM0VasAY80M0TSxddfVWAAAbkqbE3xkKHHHH69E8dO84J5ygBBBvtc6LWihiiC1rYS1ZU1saGEUTQnzRKU1bD8dTokVpreWIs7.UQeas0FprxJY2y74yGLa1L6dszmkangFfc6c5K2tb4BUVYknnhJRF84xkKVBFvlMaAkh45MfWp7TZTwiu6TGF99SKUL0LhG0XwN1zM6mAnGA+Z1jGfiH4pH.DdPnNmNmD0cxwANH.zYtTU7Cm.ae9.Q.viO7eM1nvZuAc38Nk3OjmwnGLl+DGBzv16vAB49BOt4a9lwS8TOkrx5p23PqVs3e7O9GxxgdbbbnolZBOxi7H3Nti6.uzK8R3Dm3Dxdf1gCGnnhJBaXCa.e2u62EyYNyAm5TmRVaqWud7Zu1qgwLlwf8rm8Hy3ZRN4jCY7Wrm71Qz5r3EuXYF3Ras0F16d2K1yd1iL0gNwINQL4IO4P1F8TPiEnzqqlZpAG9vGlc9.Yl1byMiSbhSfVasUTas0JidZs0VQwEWrrG30oSGToREZqs1PLwDCabc8qecTe80yp2UtxUvUtxUvfG7fwUu5Ukw.r5pqFm5TmBtc6F0VasxXhUWc0ghKtXY+.r81aOn8PS5h4.hKNe7ieb.Ht+mEUTQLFyNb3.W5RWBwGe7n81aGW+5Wm0NM0TSPsZ0H5niFpUq1uwkINOURIkfqbkqHKz2Ue80yluZt4lYyWDBA0VasvqWufPHvkKW3BW3BH1XiEd85kwvLTuMdiM1HJt3hkM+IHHfKcoKgyd1yBmNcxNmOe9PiM1Hb5zILXv.tvEtPPL5kBylMiRJoDX0pU30qWvwwgnhJJX0pUnRkJnSmN1b4ku7kQYkUF74yGNwINgLlem+7mGs1ZqHt3hCW5RWB1saGBBBvoSmnjRJAQEUTn81aGczQGLloUVYkPud8n81aGkTRIxlCaqs1fa2twku7kYLMZu81QKszBLZzHhLxHY8cGczApqt5PrwFKZokVXu.iWudQiM1HZt4lAfn6Qc4KeY31saXznQ1yLBBBviGOvqWurm073wCJu7xwku7kkMecsqcM31saXvfAb9yedYL3N8oOMrZ0JrYyFtvEt.KPWHHHfqcsqwN1mOe3K9hu.UUUUvgCGxdd65W+5xhlTz4E5KVRQLwDCrZ0Jb3vALXv.atnpppBZznApUqFW7hWT1ucnLFoA8BNNNDczQCmNcBe97A850yZGylMypmff.N8oOMZu81QrwFKJpnhXiaWtbgSbhSfpqtZX0pUYOi1agbmmiPvnRNZXZvFvopvB9mmpFLwTiE6Z1ofG7yZAGvrCPzvKWfNNh789SV6I55dbBb90BpeldBbhRw4U7pV0DLhWdpFweZWW.m3xVvruggfkNyz8awkD+F4IGHDt9iwd1s3m+y+4Xu6cu3BW3BxTCPfKPSeasgO7gi2+8ee7vO7CiBKrPY06Tm5T3Tm5TH5niFQEUTLoJ850KZqs1BqiomPBIfW60dMr3EuXHHHfMsoMI67cUFbn6.ktG6XGKl1zlFN1wNF6babiajEtyni4EtvExx3E8UL0oNUjQFYfJpnBVYaYKagI8ZfK9pVsZLnAMHTZokBud8B0p67QTylMijRJIbsqcMjZpoBdddDYjQhAO3AC.vRvtz1QJZt4lwHFwHPhIlH5niNPiM1HKfFDarwhFarQTVYkA0pUynIe97g5qudDWbwgqd0qJyvXBjtCz5f6niNfFMZXpmtt5pCNb3.FMZDVsZEQGczHwDSDIlXhAEGWat4lgACFPTQEEqeZt4lgJUpfOe9PyM2Li10pUKFxPFBJszRCJSfvyyyzzQ6s2NLXv.RIkTPJojRWZ3QW6ZWCCdvCF0TSMr4YUpTwRyVUUUUPkJUrwcDQDAFxPFBRM0TgUqVgUqVCoAi4vgCzZqshniNZzbyMyteEWbwAiFMhjSNYVVRQPP.QGczvgCGn95qGFMZDZ0pE777viGOn81aGSe5SGZ0pEM0TSvpUqHkTRAwDSLvjISnwFajQCpToBpToBZznAs2d6PPPf8LCEkTRIrmYjduj9rhACFjkiM850Kt10tF5niNPBIjPHsNbMZz.GNbfINwIBsZ0BWtbABQLH6G37SiM1HTqVM73wCrYylLq7M93iGCcnCEM1Xin81aGZ0pEBBBrmk..LXvfrmIk97uOe9vvF1vXLrkpgKouDLfnOEe0qdU31salg.xwwAiFMhXiMVDUTQIKS1PegeZePoAWtbgVZoE1KalYlYBdddDWbwgAMnAAsZ0hXhIF1ZlRoCOd7.Od7fwMtwAUpTIabSHDjXhIxzPPuQacABYibpDaZ0nBlutC3wm.duunZ7566RX8SNJ7KuwXEYb4g.h..G36zwzCmbYDNP374uN7flQ3gGADqNUXCyJd7aGiNrqiaFu+oqAQnVEl1HiGwEoXFhfx3yei0mGn8DjPBIf+ze5OIigW2oluwLlwfcu6cie1O6mExe.zVasg5pqNTQEUfJpnBX1r4vx36tu66FG9vGlYMom9zmNH0PFnO40SPf6WmVsZCR0mW5RWB0TSMrwtd85CJHU2WPhIlHyuIo8egEVHSUhAN+51saLzgNTXznQb0qdU103vgCFSAud8JS0od85UlpCokI8sPiM1XQs0VKb4xEawTJ74yGF6XGKZt4lQyM2L6Gh0TSMvqWuH5niloRQJ73wCb4xUPLtnHhHhfodPKVrfN5nC1BdTIcnp1gF54.DWzOojRBCaXCCZ01ousVas0BMZz.CFLfqd0qxl2b4xEF5PGJhLxHQEUTgrE8jFx6zqWOZqs1fc61QM0TCSxu.uGX2tc1ab6zoyfTqzDlvDfYylkohJud8hN5nC3xkKX0pUYzsTTWc0wXpQuWHsMjNWRHDDQDQfnhJJTbwEijRJI13QsZ0PmNcL0UawhE1bK8dRlYlIZs0VkIguff.hJpnvDlvDBJV2NgILAbK2xsvXpSQTQEELYxjLKBWPP.Z0pEomd5PsZ0AEZAoO2wwwAdddzPCMf1ZqMTRIkDxXbKgPv0t10fACFfFMZvku7kY2SnRyZ2tc3zoS1h8BBBH1XiEVrXAVsZEwFarx9MfTZRPPf8hXkVZovsa2xNmz6+s0VaX7ie7Pud83pW8pxnSppZCj1SO8zgACFj0tM1XivqWuHpnhBM1XixtWKscn+9VJ8pQiFnSmNlJjc3vAhJpnX0SiFMXDiXD3JW4JAMW1afpW3EdgWfd.G5z1ttgTLhgFmdbvKH9vyEppY7SuwAgEjQT3xN8gpayqHmIQMmh.3iJ1dT2xiC9YRB.uBfiWEV7vMfMMy3vMaffmeGW.u0mVAFV7Fve4GNYbio2YfQtmr4t4latxz27hVzhBoUW1SvnG8nQ4kWdPFtx3F23vhW7hC4djXvfALu4MOL24NWlZEBL9VFNnWudbG2wcf0st0ge6u82J6sKeoW5kj4KeojRJ3+8+8+sS0g0GLRDo86F1vFBa8m9zmNKUO0WAkFEDDjk44c61MhM1Xwsca2VPzka2tQDQDALYxD5niNPxImLznQCZs0VgNc5vXFyXPzQGMZqs1XRNP+ArTFZToFoRqXznQb8qecTUUUgXhIFLxQNRVe2QGcfAMnAwTIHUpplatYjZpohzSOcHHH.0pUyVjswFajwzh1uRumnVsZnWudb4KeYXwhELtwMN1Ofou4NcOaRM0TYu3jCGNPCMz.S0TzE0Zt4lw3F23vvF1vXRFnVsZ31sanSmNjVZoAGNbfgLjgfHhP7EGc4xEqdZznABBBnhJp.Nb3HHlqT5tkVZAQEUTXDiXDPmNcr6U.h9pYRIkDzqWO344kI8TUUUEZrwFQBIj.F1vFVPOC.Ht2ZiXDi.lLYBtb4B50qmQCtb4BwDSLLZmiiCtc6FCZPCB5zoCwEWbPmNcLIbhKt3P0UWMpolZPJojBF5PGJ.Dkfn4laFUUUUvfACX3Ce3rWHfNWGHyYWtbgHiLRnVsZljbTZu95qGVsZEs1ZqXPCZPPsZ0L0nRaKa1rwdVzkKWxjZOpnhBUVYkn95qGIkTRr4RoyKtc6F1saGSbhSDIjPBLMDnRkJ30qWTWc0gqe8qiTSMU1bNgPfOe9PbwEGzqWOhN5nQjQFI64HppVUoRESMqlLYhI0I8YVOd7vlWo2iqrxJgWudYL0j9aS850ydNlV+DSLQjTRIg5qudDe7wCUpTw9sSZokFHDwfxgzWPQud8xZa58ec5zANNNDYjQhJqrRzPCMfQMpQI62Xd85EojRJPiFMHxHirOGSZ86pCRAg41BDBvoqzB9rRuNN6UaFiHwnwTGdbXZiNA7AM4Ea2rSbn5bAqtD8OO.H5X6Rgf+lWMGhMBUXIlzgkMbCXzZ7g8e15v6chpQsM6.y+FGJ9kKbrHIi5An40O.P76c8b9omPsd+m7IeBZqs1fffXrGcxSdx8qLrds0VKN4IOIq8.Dyh18TFpkWd43XG6X3rm8r3rm8rn95qmsXlACFPZokFFwHFAtoa5lvzm9zwMey2bHamSe5SCylM2mng.QfLKEDDvgNzgBhIMcLOtwMtfrnz9Jb4xE93O9iga2tEiMrBBH8zSG2zMcSCHseWgAZyj9+Dt9v+NwWEootBcmq.H0fPBTkd811q+fvY5+eca9Nb3K6ww+N5uPv7KX3wm.psEm3Y214faeDXJdCXLIGMtkQMXnJFC3DM6Em1hGTuSungN7wxF7QqAHQcpvHhVClVBQf6LQsvsiNvYpnYbFyVggHTiyatEL+INTLuaLY+Y38NY880+GQDAcyvADeyzdSXBSAJPAJPAC7nGw7C9cB9Fr1AxuvqByVb.Swa.s6xK75ifGJqTgiN7gjRHJ.+psn4VriAEWjvnZfxpqUTrYqvhcuXJYDGNwks.uBBvndM31FWhXbCyHDY4wwbQPEn.En.Enf+cgtk4mXH2Tt9FKut1PcVcBaN7fW+fhllaC15.ieXw.CZ0fwOrXPys6F+fuSZnnpshZrZGG3rMfwLzXve3GLQTvYpEwnWClSlIBJmNRmclDFfJbAUfBTfBTv.O5dI+XQcZ5whtsf.wG7IvgiTbinxFaGls3.FzpBF0qAW7Zsh4OojwsNtgfxpqUrsO2Ll+jRF2xHimoZSwllpdSVHz1+2HeCRomJPAJPAJ3qZnGn1Shn+0AefvwKxRxuAwDHZ0oGnUMOr5vMhRuFDgJdINodmsG.MyP.IQAMotMOMGPnv.TAJPAJPAC7nGtmeJPAJPAJPAeyA877NjBTfBTfBTv2PfByOEn.En.E7sNnv7SAJPAJPAeqCJL+TfBTfBTv25fByOEn.En.E7sN7+GrbhIp4JQWnO.....IUjSD4pPfIH" ]
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "ubutton",
					"handoff" : "",
					"numinlets" : 1,
					"numoutlets" : 4,
					"patching_rect" : [ 192.0, 138.0, 301.0, 51.0 ],
					"outlettype" : [ "bang", "bang", "", "int" ],
					"id" : "obj-30"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "panel",
					"border" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"bgcolor" : [ 0.956863, 1.0, 0.419608, 1.0 ],
					"patching_rect" : [ 367.0, 136.0, 126.0, 56.0 ],
					"id" : "obj-31",
					"rounded" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"source" : [ "obj-2", 0 ],
					"destination" : [ "obj-1", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-19", 0 ],
					"destination" : [ "obj-16", 1 ],
					"hidden" : 0,
					"midpoints" : [ 317.5, 554.0, 444.5, 554.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-4", 0 ],
					"destination" : [ "obj-24", 2 ],
					"hidden" : 1,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-1", 0 ],
					"destination" : [ "obj-4", 0 ],
					"hidden" : 1,
					"midpoints" : [ 603.5, 200.0, 411.5, 200.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-20", 0 ],
					"destination" : [ "obj-16", 0 ],
					"hidden" : 0,
					"midpoints" : [ 227.5, 560.0, 372.5, 560.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-16", 0 ],
					"destination" : [ "obj-17", 0 ],
					"hidden" : 1,
					"midpoints" : [ 372.5, 598.0, 538.0, 598.0, 538.0, 252.0, 356.5, 252.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-5", 0 ],
					"destination" : [ "obj-24", 1 ],
					"hidden" : 1,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-18", 2 ],
					"destination" : [ "obj-19", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-21", 0 ],
					"destination" : [ "obj-14", 1 ],
					"hidden" : 0,
					"midpoints" : [ 272.5, 563.0, 299.5, 563.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-18", 1 ],
					"destination" : [ "obj-21", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-20", 0 ],
					"destination" : [ "obj-14", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-18", 0 ],
					"destination" : [ "obj-20", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-27", 0 ],
					"destination" : [ "obj-18", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-24", 0 ],
					"destination" : [ "obj-27", 0 ],
					"hidden" : 0,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-25", 0 ],
					"destination" : [ "obj-24", 0 ],
					"hidden" : 1,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-14", 0 ],
					"destination" : [ "obj-15", 0 ],
					"hidden" : 1,
					"midpoints" : [ 227.5, 600.0, 96.0, 600.0, 96.0, 250.0, 211.5, 250.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-27", 0 ],
					"destination" : [ "obj-6", 0 ],
					"hidden" : 0,
					"midpoints" : [ 227.5, 498.0, 135.5, 498.0 ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-30", 0 ],
					"destination" : [ "obj-3", 0 ],
					"hidden" : 1,
					"midpoints" : [ 201.5, 205.0, 43.0, 205.0, 43.0, 40.0, 71.5, 40.0 ]
				}

			}
 ]
	}

}
